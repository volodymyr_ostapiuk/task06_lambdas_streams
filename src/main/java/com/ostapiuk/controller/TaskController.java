package com.ostapiuk.controller;

import com.ostapiuk.model.task1.Task1;
import com.ostapiuk.model.task2.Task2;
import com.ostapiuk.model.task3.Task3;
import com.ostapiuk.model.task4.Task4;
import com.ostapiuk.view.UserInterface;

public class TaskController {
    private UserInterface view;
    private String result;

    public TaskController(UserInterface userInterface) {
        view = userInterface;
    }

    public final void getTaskAaResult() {
        Task1 taskA = new Task1();
        result = "This is the result of Task 1.a. "
                + "Max value of (8, 17, 4): ";
        updateView(taskA.getMax().calculate(8, 17, 4));
    }

    public final void getTaskAbResult() {
        Task1 taskA = new Task1();
        result = "This is the result of Task 1.b. "
                + "Average of (9, 2, 5): ";
        updateView(taskA.getAvg().calculate(9, 2, 5));
    }

    public final void getTaskBResult() {
        Task2 taskB = new Task2();
        taskB.startTask2();
    }

    public final void getTaskCResult() {
        Task3 taskC = new Task3();
        taskC.startTask3();
    }

    public final void getTaskDResult() {
        Task4 taskD = new Task4();
        taskD.startTask4();
    }

    private void updateView(int lambdaResult) {
        view.printResult(lambdaResult, result);
    }
}
