package com.ostapiuk.view;

import com.ostapiuk.controller.TaskController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInterface {
    private TaskController controller;
    private static Logger logger = LogManager.getLogger();

    public UserInterface() {
        controller = new TaskController(this);
    }

    private void showMenu() {
        System.out.println("1 - Task A.a");
        System.out.println("2 - Task A.b");
        System.out.println("3 - Task B");
        System.out.println("4 - Task C");
        System.out.println("5 - Task D");
        System.out.println("6 - For exit");
        System.out.print("Press number from 1 to 6: ");
    }

    public final void getUserChoice() {
        boolean check = false;
        do {
            showMenu();
            int choice = enterNumber();
            switch (choice) {
                case 1:
                    controller.getTaskAaResult();
                    break;
                case 2:
                    controller.getTaskAbResult();
                    break;
                case 3:
                    controller.getTaskBResult();
                    break;
                case 4:
                    controller.getTaskCResult();
                    break;
                case 5:
                    controller.getTaskDResult();
                    break;
                case 6:
                    check = true;
                    break;
                default:
                    break;
            }
        } while (!check);
    }

    /**
     * Method allows to enter only numbers.
     *
     * @return input number by user
     */
    private int enterNumber() {
        int numberInput = 0;
        try {
            Scanner input = new Scanner(System.in, "UTF-8");
            numberInput = input.nextInt();
            return numberInput;
        } catch (InputMismatchException exception) {
            System.out.print("Please enter only numbers here: ");
            enterNumber();
        }
        return numberInput;
    }

    public final void printResult(int numResult, String result) {
        logger.info(result);
        logger.info(numResult);
    }
}
