package com.ostapiuk.model.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Task4 {
    private static List<String> stringList = new ArrayList<>();
    private static Logger logger = LogManager.getLogger();

    public void startTask4() {
        System.out.println("Enter some number of text lines:");
        Scanner scanner = new Scanner(System.in);
        String s;
        for (; ; ) {
            s = scanner.nextLine();
            if (s.isEmpty()) {
                break;
            }
            stringList.add(s);
        }
        countUniqueWords();
        countOccurrence();
    }

    private void countUniqueWords() {
        logger.info("Number of unique words: "
                + stringList.stream().distinct().count());
        logger.info("Sorted list of all unique words:");
        stringList.stream().distinct()
                .sorted().forEach(x -> logger.info(x + " "));
    }

    private void countOccurrence() {
        logger.info("\nWord count. Occurrence number of each word in the text:"
                + stringList.stream().collect(Collectors
                .groupingBy(x -> x, Collectors.counting())));

        logger.info("Occurrence number of each symbol except upper case characters:"
                + stringList.stream().flatMap(x -> x.chars().boxed())
                .map(x -> (char) x.intValue()).collect(Collectors
                        .groupingBy(x -> x, Collectors.counting())));
    }
}
