package com.ostapiuk.model.task1;

public class Task1 {
    public CalculateInterface getMax() {
        return (a, b, c) -> Math.max(a, Math.max(b, c));
    }

    public CalculateInterface getAvg() {
        return (a, b, c) -> (a + b + c) / 3;
    }
}
