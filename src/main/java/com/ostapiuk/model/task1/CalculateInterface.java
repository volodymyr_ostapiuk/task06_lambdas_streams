package com.ostapiuk.model.task1;

@FunctionalInterface
public interface CalculateInterface {
    int calculate(int var1, int var2, int var3);
}
