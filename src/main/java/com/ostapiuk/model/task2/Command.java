package com.ostapiuk.model.task2;

@FunctionalInterface
interface Command {
    void invoke(String arg);
}
